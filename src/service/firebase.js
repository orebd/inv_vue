// import firebase from 'firebase/app'
import Firebase from 'firebase/app';

import 'firebase/firestore';
import 'firebase/auth';
// import storeFb from '@/store/firebaseAuth';

// // export const db = firebaseApp.firestore();
// export default {
//   db: firebase.firestore(),
// };

// Get a Firestore instance
const firebase = Firebase
  .initializeApp({
    apiKey: process.env.VUE_APP_FIREBASE_apiKey,
    authDomain: process.env.VUE_APP_FIREBASE_authDomain,
    databaseURL: process.env.VUE_APP_FIREBASE_databaseURL,
    projectId: process.env.VUE_APP_FIREBASE_projectId,
    storageBucket: process.env.VUE_APP_FIREBASE_storageBucket,
    messagingSenderId: process.env.VUE_APP_FIREBASE_messagingSenderId,
    appId: process.env.VUE_APP_FIREBASE_appId,
    measurementId: process.env.VUE_APP_FIREBASE_measurementId,
  });
  // .firestore()

// utils
export const db = firebase.firestore();
export const auth = firebase.auth();

// collection references
export const config = db.collection('config');
// export const postsCollection = db.collection('posts');
// export const commentsCollection = db.collection('comments');
// export const likesCollection = db.collection('likes');
