import Vue from 'vue';
import VueRouter from 'vue-router';
import { auth } from '@/service/firebase';
import EmailVerification from '@/components/EmailVerification.vue';
import Login from '@/components/LoginForm.vue';
import storeFb from '@/store/firebaseAuth';
import Config from '@/views/Config.vue';
import Account from '@/views/Account.vue';
import Home from '../views/Home.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    meta: { requiresAuth: true },
  },
  {
    path: '/login',
    name: 'Login',
    component: Login,
  },
  {
    path: '/emailverification',
    name: 'EmailVerification',
    component: EmailVerification,
  },
  {
    path: '/config',
    name: 'Config',
    component: Config,
    meta: { requiresAuth: true },
  },
  {
    path: '/account',
    name: 'Account',
    component: Account,
    meta: { requiresAuth: true },
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue'),
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach((to, from, next) => {
  auth.onAuthStateChanged((user) => {
    if (to.matched.some((record) => record.meta.requiresAuth)) {
      if (!user) {
        storeFb.isAuthenticated = false;
        storeFb.emailVerified = false;
        next({ path: '/login' });
      } else if (user && !user.emailVerified) {
        storeFb.isAuthenticated = true;
        storeFb.emailVerified = false;
        next({ path: '/emailverification' });
      } else if (user && user.emailVerified) {
        storeFb.isAuthenticated = true;
        storeFb.emailVerified = true;
        next();
      } else next();
    } else if (user && user.emailVerified && to.name === 'Login') {
      storeFb.isAuthenticated = true;
      storeFb.emailVerified = true;
      next('/');
    } else {
      next();
    }
  });
});

export default router;
